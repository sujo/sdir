#!/bin/sh

[ -n "$1" ] && DIR="$1" || DIR="."

printf "<!DOCTYPE html> \
	<html> \
	<head> \
	<title>Index of ${DIR##*/}</title> \
	<body> \
	<h1>Index of ${DIR##*/}</h1> \
	<hr> \
	<pre> \
	<ul style=\"list-style-type:none\">"

for entry in $DIR/*
do	
	stat="$(ls -p1lLAhd $entry)"	
	[ -d $entry ] && entry="$entry/"	
	echo "<li>${stat%%$entry}<a href=\"$DIR/${entry##$DIR/}\">${entry##$DIR/}</a></li>"
done

printf "</ul> \
	</pre> \
	</body> \
	</title>"
